//
//  FavoriteTableViewController.swift
//  searchGitRepos
//
//  Created by Александр Волков on 03.06.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class FavoriteTableViewController: UITableViewController {
    
// MARK: Properties
    
    var reps = [GitObject]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
// MARK: Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reps = ServerManager.shared.favoriteReps
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

// MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.reps.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FavoriteCell", for: indexPath)
            cell.textLabel?.text = self.reps[indexPath.row].fullName
        

        return cell
    }
    

}
