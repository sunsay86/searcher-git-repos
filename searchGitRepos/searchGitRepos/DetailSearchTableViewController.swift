//
//  DetailSearchTableViewController.swift
//  searchGitRepos
//
//  Created by Александр Волков on 03.06.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class DetailSearchTableViewController: UITableViewController {
   
    
// MARK: Properties
    var repo = GitObject() {
        didSet{
            
        }
    }
    var email: String?{
        didSet {
            self.emailLabel.text = email ?? "No e-mail"
            self.tableView.reloadData()
        }
    }
    
// MARK: Outlets
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var ownerLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
// MARK: Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameLabel.text = repo.fullName
        self.descriptionLabel.text = repo.description
        self.ownerLabel.text = repo.owner
        self.emailLabel.text = repo.email
        let favoriteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "Like"), style: .done, target: self, action: #selector(addFavoriteAction))
        self.navigationItem.rightBarButtonItem = favoriteButton
        if self.repo.isFavorite{
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.blue
        } else {
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
        }
}
    
    
// MARK: Favorite Button Action
    func addFavoriteAction() {
        
        
        if self.navigationItem.rightBarButtonItem?.tintColor == UIColor.white{
            
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.blue
            
            self.repo.isFavorite = true
            
        } else {
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor.white
            
            self.repo.isFavorite = false
        }
        
        if !ServerManager.shared.favoriteReps.isEmpty {
            
            let filteredReps = ServerManager.shared.favoriteReps.filter{$0.fullName == self.repo.fullName}
            
            if filteredReps.isEmpty{
                ServerManager.shared.favoriteReps.append(self.repo)
            } else {
                for (index, object) in ServerManager.shared.favoriteReps.enumerated(){
                    if object.fullName == self.repo.fullName{
                        ServerManager.shared.favoriteReps.remove(at: index)
                    }
                    
                }
                

            }
            
        } else {
            ServerManager.shared.favoriteReps.append(self.repo)
        }
            
        
            
    }
}
    

