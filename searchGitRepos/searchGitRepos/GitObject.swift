//
//  GitObject.swift
//  searchGitRepos
//
//  Created by Александр Волков on 03.06.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation

class GitObject {
    
    var fullName = String()
    var description = String()
    var owner = String()
    var email = String()
    var isFavorite = false
   
}


