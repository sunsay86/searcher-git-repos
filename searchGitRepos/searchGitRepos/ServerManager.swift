//
//  ServerManager.swift
//  searchGitRepos
//
//  Created by Александр Волков on 03.06.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ServerManager {
    
   
    
    private init(){}
    
    static let shared = ServerManager()
    
    // MARK: Properties
    var favoriteReps = [GitObject](){
        didSet{
            self.saveToFile()
        }
    }
    
    // https://api.github.com/search/repositories?q=tetris
    
    // MARK: Local methods
    //saveTofile
    
    private func saveToFile() {
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("GitObjects.json")
        
        var favorites = [Dictionary<String, Any>]()
        
        for object in favoriteReps{
            let dictionary: [String: Any] = ["full_name" : object.fullName,
                                             "description" : object.description,
                                             "owner" : object.owner,
                                             "email" : object.email,
                                             "isFavorite" : object.isFavorite]
            
            favorites.append(dictionary)
            
        }
        
        do {
            let json = JSON(favorites)
            let str = json.description
            let data = str.data(using: String.Encoding.utf8)
            try data?.write(to: fileUrl, options: [])
            
        } catch {
            print(error.localizedDescription)
        }

    }
    // getFromFile
    
    func getFromFile()-> [GitObject]{
        
        guard let documentDirectoryUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return [GitObject]() }
        let fileUrl = documentDirectoryUrl.appendingPathComponent("GitObjects.json")
        
        var gits = [GitObject]()
        
        do {
            let data = try Data(contentsOf: fileUrl, options: [])
            let favorites = JSON(data: data)
            
            for (_, object) in favorites {
                let git = GitObject()
                
                git.fullName = object["full_name"].stringValue
                git.description = object["description"].stringValue
                git.owner = object["owner"].stringValue
                git.email = object["email"].stringValue
                git.isFavorite = object["isFavorite"].boolValue
                
                
                
                gits.append(git)
            }
            
        } catch {
            print(error.localizedDescription)
        }
        
        
        return gits
        
    }
    
    
// MARK: Server methods
    func searchRepsWith(key: String,
                  onSuccess: @escaping ([GitObject]) -> Void,
                  onFailure: @escaping (Error) -> Void){
        
        Alamofire.request("https://api.github.com/search/repositories", method: .get, parameters: ["q": key])
            .responseJSON { response in
                
                switch(response.result) {
                case .success(_):
                    var gits = [GitObject]()
                    if response.data != nil{
                        
                        let json = JSON(data: response.data!)
                        let items = json["items"]
                        
                        
                        for (_, object) in items {
                            let git = GitObject()
                            git.fullName = object["full_name"].stringValue
                            print(git.fullName)
                            git.description = object["description"].stringValue
                            print(git.description)
                            git.owner = object["owner"]["login"].stringValue
                            print(git.owner)
                                                       
                            
                            gits.append(git)
                            
                        }
                        onSuccess(gits)
                        print(gits.count)
                        break
                    }
                    
                    
                case .failure(_):
                    print(response.result.error!)
                   
                        onFailure(response.result.error!)
                    
                    
                    break
                    
                }
        }
        
        
    }

        
    
    
  
        
        
        
    func getASingle(user: String,
                    onSuccess: @escaping (String) -> Void,
                    onFailure: @escaping (Error) -> Void)
    {
        
        Alamofire.request("https://api.github.com/users/\(user)")
            .responseJSON { response in
                
                switch(response.result) {
                case .success(_):
                    if response.data != nil{
                        let json = JSON(data: response.data!)
                       onSuccess(json["email"].stringValue)
                       print(json["email"].stringValue)
                        break
                    }
                    
                case .failure(_):
                    print(response.result.error!)
                    onFailure(response.result.error!)
                    break
                    
                }

        
       }
    

    }

}
