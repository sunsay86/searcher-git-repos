//
//  SearchTableViewController.swift
//  searchGitRepos
//
//  Created by Александр Волков on 03.06.17.
//  Copyright © 2017 Александр Волков. All rights reserved.
//

import UIKit

class SearchTableViewController: UITableViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    
// MARK: Properties
    var repos = [GitObject]() {
        didSet{
            self.tableView.reloadData()
        }
    }
    
// MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.titleView = searchBar
        self.searchBar.delegate = self
        
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
// MARK: Search Reps
    func searchResultWith(text: String){
        
        
        ServerManager.shared.searchRepsWith(key: text,
                                            onSuccess: { (reps) in
                                                self.repos = reps
                                                
        }) { (error) in
            print(error.localizedDescription)
            
            
            let alert = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            let retryAction = UIAlertAction(title: "Retry", style: .default, handler: { (action) in
                self.searchResultWith(text: self.searchBar.text!)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            alert.addAction(cancelAction)
            alert.addAction(retryAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
// MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return repos.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath)
                cell.textLabel?.text = self.repos[indexPath.row].fullName
         

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let repo = self.repos[indexPath.row]
         let dvc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailSearchVC") as! DetailSearchTableViewController
        ServerManager.shared.getASingle(user: repo.owner,
                                        onSuccess: { (email) in
                                            dvc.email = email
                                            print("\(email) in did select")
                                            
                                            
        }) { (error) in
            repo.email = ""
        }
        
        let filteredReps = ServerManager.shared.favoriteReps.filter{$0.fullName == repo.fullName}
        if !filteredReps.isEmpty{
            repo.isFavorite = true
        }
        
        dvc.repo = repo
        self.navigationController?.show(dvc, sender: tableView)
        
        
    }
    
}
// MARK: search bar delegate

extension SearchTableViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        if !searchBar.text!.isEmpty {
            searchBar.resignFirstResponder()
            searchResultWith(text: searchBar.text!)
            
        }
    }
}
